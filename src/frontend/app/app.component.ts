import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { CountryDetails } from './country-details.model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

    public countries: CountryDetails[] = [];

    constructor(
        private readonly changeDetector: ChangeDetectorRef
    ) { }

    public ngOnInit(): void {
        window.displayCountries = this.displayCountries.bind(this);

        // tslint:disable-next-line:ban-ts-ignore
        // @ts-ignore
        import('./search/naive');
        // import('./search/imperative');
        // import('./search/reactive');
    }

    public displayCountries(countries: CountryDetails[]): void {
        this.countries = countries;
        this.changeDetector.detectChanges();
    }

}
