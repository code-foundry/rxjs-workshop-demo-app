import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators'

const queryElement = document.querySelector('#search-input');

fromEvent(queryElement, 'keyup').pipe(

	map((keyUpEvent) => keyUpEvent.target.value),

	debounceTime(250),

	filter((query) => query.length >= 2),

	distinctUntilChanged(),

	map((query) => `countries/${encodeURIComponent(query)}`),

	switchMap((requestUrl) => {
		const xhr = new XMLHttpRequest();
		xhr.open('GET', requestUrl);
		xhr.send();
		return fromEvent(xhr, 'load');
	}),

	filter((xhrEvent) => xhrEvent.currentTarget.status === 200),

	map((xhrEvent) => {
		return JSON.parse(xhrEvent.currentTarget.responseText);
	}),

).subscribe(displayCountries);
