const queryElement = document.querySelector('#search-input');

let lastRequestId = 0;
let lastTimeoutId = null;
let lastQuery = null;

queryElement.addEventListener('keyup', (keyUpEvent) => {
	const query = keyUpEvent.target.value;

	if (lastTimeoutId) {
		clearTimeout(lastTimeoutId);
	}

	lastTimeoutId = setTimeout(() => {

		lastTimeoutId = null;

		doSearch(query);

	}, 250);
});

function doSearch(query) {

	if (query.length < 2 || lastQuery === query) {
		return;
	}

	lastQuery = query;

	const requestUrl = `countries/${encodeURIComponent(query)}`;

	const xhr = new XMLHttpRequest();
	xhr.open('GET', requestUrl);
	xhr.send();

	const requestId = ++lastRequestId;

	xhr.addEventListener('load', () => {
		if (requestId !== lastRequestId) {
			return;
		}

		if (xhr.status === 200) {
			const countries = JSON.parse(xhr.responseText);

			displayCountries(countries);
		}
	});

}
