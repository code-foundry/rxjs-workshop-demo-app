const queryElement = document.querySelector('#search-input');

queryElement.addEventListener('keyup', (keyUpEvent) => {
    const query = keyUpEvent.target.value;

    doSearch(query);
});

function doSearch(query) {

    const requestUrl = `countries/${encodeURIComponent(query)}`;

    const xhr = new XMLHttpRequest();
    xhr.open('GET', requestUrl);
    xhr.send();
    xhr.addEventListener('load', () => {
        if (xhr.status === 200) {
            const countries = JSON.parse(xhr.responseText);

            displayCountries(countries);
        }
    });
}
