import { CountryDetails } from './country-details.model';

declare global {
    interface Window {
        displayCountries(countries: CountryDetails[]): void;
    }
}
