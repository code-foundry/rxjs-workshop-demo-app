const http = require('http');

const serverPort = 4100;

const responseDelayRange = { min: 0, max: 2000 };

// Load countries.
console.log('Loading country database...');

const countries = require('./countries.json');

console.log(`Loaded ${countries.length} countries`);

// Create HTTP server.
const server = http.createServer((request, response) => {

    if (request.method !== 'GET' || !request.url.startsWith('/countries/')) {
        response.writeHead(404);
        response.end();
    }

    const query = (request.url.substr('/countries/'.length) || '').toLowerCase();

    const results = countries.filter((country) => country.name.toLowerCase().includes(query));

    const responseDelay = Math.round(Math.random() * (responseDelayRange.max - responseDelayRange.min) + responseDelayRange.min);

    setTimeout(() => {
        response.writeHead(200, { 'Content-Type': 'application/json' });
        response.end(JSON.stringify(results));
    }, responseDelay);

});

// Launch HTTP server.
console.log('Starting backend...');
server.listen(serverPort, () => {
    console.log(`Backend HTTP server listening on port ${serverPort}`);
});
