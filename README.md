# RxJS workshop demo application

This repository contains the demo application that was shown at the [RxJS workshop](https://gitlab.com/code-foundry/rxjs-workshop).

## Running the demo application

Use the following steps to run the application:

* Make sure you've got Node.js version 12 or higher installed.

* Download or clone this repository

* Open a console and navigate to the directory where you've copied this repository.

* Execute `npm install` to download the dependencies for the project.

* Execute `npm start` to start the application.

* Open a browser and open the following URL: [http://localhost:4200/](http://localhost:4200/)

You should now be able to see the demo application.

## About the demo application

This demo application provides a simple search interface for country names.
There are three different implementations of the search client:

* A _naive_ implementation.
This implementation uses a 'traditional' imperative programming style, without worrying about debouncing the input, checking for changed inputs or out of order server responses.
The source can be found in [`src/frontend/app/search/naive.js`](src/frontend/app/search/naive.js).

* A _traditional_ implementation. Same as the naive implementation, but having taken care of the problems outlined above.
The source can be found in [`src/frontend/app/search/imperative.js`](src/frontend/app/search/imperative.js).

* A _reactive_ implementation. This version also has taken care of the problems above, but is implemented instead using RxJS.
The source can be found in [`src/frontend/app/search/reactive.js`](src/frontend/app/search/reactive.js).

To switch between these implementations uncomment the desired implementation in [`src/frontend/app/app.component.ts`](src/frontend/app/app.component.ts).

Note this application uses Angular just as scaffolding.
It could just a well have been any other framework like React or Vue.
The actual search implementations are just vanilla Javascript, to illustrate that the concepts and benefits of Reactive Programming are independent of the used framework (and programming language).

## Acknowledgments

For this demo we have used a JSON file (maintained by [lukes](https://github.com/lukes)) that contains the ISO-3166 country list.
This JSON file was copied from the following repository:

[https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes](https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes)
